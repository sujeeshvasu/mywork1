<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class HelloWorldTest extends TestCase {

  private $pdo;

  protected function setUp() : void {
    $this->pdo = new PDO($GLOBALS['db_dsn'], $GLOBALS['db_username'], $GLOBALS['db_password']);
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->pdo->query("CREATE TABLE hello (what VARCHAR(50) NOT NULL)");
  }

  protected function tearDown() : void {
    if (!$this->pdo)
      return;
    $this->pdo->query("DROP TABLE hello");
  }

  public function testHelloWorld() {
    $helloWorld = new HelloWorld($this->pdo);
    $this->assertEquals('Hello World', $helloWorld->hello());
  }

  public function testHello() {
    $helloWorld = new HelloWorld($this->pdo);
    $this->assertEquals('Hello Bar', $helloWorld->hello('Bar'));
  }

  public function testWhat() {
    $helloWorld = new HelloWorld($this->pdo);
    $this->assertFalse($helloWorld->what());
    $helloWorld->hello('Bar');
    $this->assertEquals('Bar', $helloWorld->what());
  }

}

?>
